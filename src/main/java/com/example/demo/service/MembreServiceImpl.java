package com.example.demo.service;


import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.demo.beans.PublicationBean;
import com.example.demo.dao.EnseignantChercheurRepository;
import com.example.demo.dao.EtudiantRepository;
import com.example.demo.dao.MembrePubRepository;
import com.example.demo.dao.MembreRepository;
import com.example.demo.entities.EnseignantChercheur;
import com.example.demo.entities.Etudiant;
import com.example.demo.entities.Membre;
import com.example.demo.entities.Membre_Pub_Id;
import com.example.demo.entities.Membre_Publication;
import com.example.demo.proxies.PublicationProxyService;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class MembreServiceImpl implements IMemberService {
	
	MembreRepository memberRepository ;
	EtudiantRepository etudiantRepository;
	EnseignantChercheurRepository enseignantchercheurRepository ;
	MembrePubRepository  membrepubrepository;
	PublicationProxyService proxy;
	
	@Override
	public Membre addMember(Membre m) {
		memberRepository.save(m) ;
		return m;
	}

	@Override
	public void deleteMember(Long id) {
		memberRepository.deleteById(id);
	}

	@Override
	public Membre updateMember(Membre p) {
		
		return memberRepository.saveAndFlush(p);
	}

	@Override
	public Membre findMember(Long id) {
		Membre m = (Membre) memberRepository.findById(id).get() ;
		return m;
	}

	@Override
	public List<Membre> findAll() {
				return memberRepository.findAll();
	}

	@Override
	public Membre findByCin(String cin) {
		
		return memberRepository.findByCin(cin);
	}

	@Override
	public Membre findByEmail(String email) {
		
		return memberRepository.findByEmail(email);
	}

	@Override
	public List<Membre> findByNom(String nom) {
		
		return memberRepository.findByNomStartingWith(nom);
	}

	@Override
	public List<Etudiant> findByDiplome(String diplome) {
		
		return etudiantRepository.findByDiplome(diplome);
	}

	@Override
	public List<EnseignantChercheur> findByGrade(String grade) {
		
		return enseignantchercheurRepository.findByGrade(grade);
	}

	@Override
	public List<EnseignantChercheur> findByEtablissement(String etablissement) {
		
		return enseignantchercheurRepository.findByEtablissement(etablissement);
	}

	@Override
	public void affecterEtudiantToEnseignant(Long idEtd, Long idEns) {
		Etudiant etd = etudiantRepository.findById(idEtd).get();
		EnseignantChercheur ens = enseignantchercheurRepository.findById(idEns).get();
		etd.setEncadrent(ens);
		etudiantRepository.save(etd);
		
	}

	public List<Etudiant> afficherEtudiant_Enseignant(EnseignantChercheur ens) {
		
		return etudiantRepository.findByEncadrent(ens);
	}

	@Override
	public void affecterauteurTopublication(Long idauteur, Long idpub) {
		Membre mbr= memberRepository.findById(idauteur).get();
		Membre_Publication mbs= new Membre_Publication();
		mbs.setAuteur(mbr);
		mbs.setId(new Membre_Pub_Id(idpub, idauteur));
		membrepubrepository.save(mbs);
		
	}

	@Override
	public List<PublicationBean> findPublicationparauteur(Long idauteur) {
		List<PublicationBean> pubs=new ArrayList<PublicationBean>();
		Membre auteur= memberRepository.findById(idauteur).get();
		List< Membre_Publication> idpubs=membrepubrepository.findByAuteur(auteur);
		idpubs.forEach(s->{
		System.out.println(s);
		pubs.add(proxy.findPublicationById(s.getId().getPublication_id()));
		});
		return pubs;
	}

}
